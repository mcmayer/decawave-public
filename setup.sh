#!/usr/bin/env bash
# Run this script the first time a C.H.I.P. SBC
# is set installing the DW1000 software

# 0. Install packages
apt-get install git make zsh vim

# 1. configure git
git config --global push.default simple
git config --global user.email mmayer@mayeranalytics.com
git config --global user.name "Markus Mayer"

# 2. create key pairs
if [ ! -f ~/.ssh/id_rsa.pub ]; then
  ssh-keygen -t rsa -b 4096 -C "mmayer@mayeranalytics.com"
fi

# 3. print the public key, add it to bitbucket
cat ~/.ssh/id_rsa.pub
echo "Press [ENTER] when the pub key is added:"
read

# 4. clone decawave
git clone git@bitbucket.org:mcmayer/decawave.git

# 5. download googletest if necessary
cd decawave
./setup.sh

# 6. copy .vimrc to ~
cd ~
cp decawave/.vimrc .
